﻿using UnityEngine;
using System.Collections;

#pragma warning disable 1717
#pragma warning disable 0414

/// <summary>
/// The game camera used to track the player
/// @author Jan Julius
/// </summary>
public class GameCamera : MonoBehaviour 
{

    /// <summary>
    /// value of targetdirection.magniteude * magnitudevalue
    /// </summary>
    private float interpVelocity;

    /// <summary>
    /// Target of the camera
    /// </summary>
    public static GameObject target;

    /// <summary>
    /// Offset compared to the targeted object
    /// </summary>
    [Header("Offset compared to the targeted object")]
    public Vector3 offset;

    /// <summary>
    /// position of target gameobject
    /// </summary>
    Vector3 targetPos;

    public float magnitudeValue = 7.5f;

    /// <summary>
    /// Speed the camera follows the target gameobject
    /// </summary>
    [Header("Speed the camera follows the player")]
    public float followSpeed = 1f;

    /// <summary>
    /// Distance the camera should have compared to the target gameobject on the x axis.
    /// </summary>
    [Header("How far the camera is compared to the player on the x axis.")]
    public float cameraDistance = 10f;

    void Start()
    {
        targetPos = transform.position;
        target = GameObject.Find("Player");
    }

    void Update()
    {
        if (target)
        {
            Vector3 posNoX = transform.position;
            posNoX.x = target.transform.position.x + cameraDistance;

            Vector3 targetDirection = (target.transform.position - posNoX);

            interpVelocity = targetDirection.magnitude * magnitudeValue;

            targetPos = targetPos = target.transform.position;//transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime);

            transform.position = Vector3.Lerp(transform.position, targetPos + offset, followSpeed);

        }
    }
}
