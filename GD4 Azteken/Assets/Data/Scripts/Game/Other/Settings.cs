﻿using UnityEngine;
using System.Collections;
using System.IO;

/// <summary>
/// variables that should be quickly callable from anywhere
/// @author Jan Julius
/// </summary>
public class Settings {

    public static string[] MonthNames = new string[] {"Januari", "Februari", "Maart", "April", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "December"};
    public static string SpritePath = "Sprites/";
    public static string EnemyPrefabPath = "Prefabs/Enemies/";
    public static string GizmosIconPath = "Gizmos/Icons/";

    /// <summary>
    /// Location of the middleblockslocation on the x axis (first value of vector3)
    /// </summary>
    public static float levelMiddleBlocksLocation = 0.02f;

}
