﻿using UnityEngine;
using System.Collections;

/// <summary>
/// represents the gamemanager manager of the game
/// @author Jan Julius
/// </summary>
[RequireComponent(typeof (GameManager))]
public class GameManager : MonoBehaviour {

    /// <summary>
    /// boolean to only run when the game runs for the first time
    /// </summary>
    static bool gameFirstRun = true;

    /// <summary>
    /// player component stores the component from the player at start of scene
    /// </summary>
    Player player;

	public void Awake () 
    {
        //makes sure this object stays in every scene
        DontDestroyOnLoad(transform.gameObject);
        if (player == null)
        {
            player = GameObject.Find("Player").GetComponent<Player>();
        }
        
        //run startup code for first time this has all default data for the player
        if (gameFirstRun)
        {
            for (int i = 0; i < 4; i++)
            {
                PlayerPrefs.SetInt("Level" + i + "Unlocked", 0);
            }
            PlayerPrefs.SetInt("Level0Unlocked", 1);
            player.SetHealth(3);
            player.SetMaxHealth(3);
            player.SetCurrency(0);
            for (int i = 0; i < 12; i++)
            {
                PlayerPrefs.SetInt("CallenderPiece" + i, 0);
            }
            SavePlayerData(player);
            //callender piece variables
            gameFirstRun = false;   
        }
	}

    void OnLevelWasLoaded()
    {
        if (player == null)
        {
            player = GameObject.Find("Player").GetComponent<Player>();
        }
        player.SetHealth(3);
        LoadPlayerData(player);
    }

    void OnApplicationQuit()
    {
        SavePlayerData(player);
    }

    void OnApplicationPause()
    {
        SavePlayerData(player);
    }

    /// <summary>
    /// saves the players data to be carried over to other seens
    /// </summary>
    /// <param name="player">player component from the player gameobject</param>
    public static void SavePlayerData(Player player)
    {
        //data for the player
        PlayerPrefs.SetInt("PlayerData0", player.GetHealth());
        PlayerPrefs.SetInt("PlayerData1", player.GetMaxHealth());
        PlayerPrefs.SetInt("PlayerData2", player.GetCurrency());
        //callender data should already be saved

        PlayerPrefs.Save();
    }

    /// <summary>
    /// Loads the player data from the gamemanager
    /// previously savaed data from saveplayerdata method
    /// </summary>
    /// <param name="player">player component from the player gameobject</param>
    public static void LoadPlayerData(Player player)
    {
        print(player);
        //data for the player
        player.SetHealth(PlayerPrefs.GetInt("PlayerData0"));
        player.SetMaxHealth(PlayerPrefs.GetInt("PlayerData1"));
        player.SetCurrency(PlayerPrefs.GetInt("PlayerData2"));
        //callenderdata is all used in MainMenu and Player check scripts for references

    }

}
