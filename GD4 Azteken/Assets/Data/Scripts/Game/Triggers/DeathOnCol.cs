﻿using UnityEngine;
using System.Collections;

public class DeathOnCol : MonoBehaviour {

    public void Start()
    {
        GetComponent<BoxCollider>().isTrigger = true;
    }

	void OnTriggerEnter(Collider other) 
	{
		if (other.name == "Player")
		{
			other.GetComponent<Player>().Die();
		}
	}
}
