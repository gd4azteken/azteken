﻿using UnityEngine;
using System.Collections;

public class Lever : MonoBehaviour 
{
    private GameObject stone;
    private int rotationSpeed = 100;
	void Awake () 
    {
        stone = GameObject.FindGameObjectWithTag("Stone");

        if(stone == null)
        {
            Debug.LogWarning("Stone not assigned to trigger", stone);
            Debug.Break();
        }
	}
    public void Update()
    {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + Time.deltaTime * rotationSpeed, transform.eulerAngles.z);
    }

    void OnTriggerEnter(Collider other) 
    {
        if (other.name == "Player")
        {
            Destroy(stone.gameObject);
            gameObject.SetActive(false);
        }
    }
}