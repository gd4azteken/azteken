﻿using UnityEngine;
using System.Collections;


public class FallingPlatforms : MonoBehaviour 
{
	public bool fall = false;
	public int speed = 20;
	public GameObject bridge;
	private Rigidbody myRigidbody;

	private void Start()
	{
		myRigidbody = bridge.gameObject.GetComponent<Rigidbody>();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.name == "Player")
		{
			StartCoroutine(DropAfter(0.3f));
		}
	}
	public IEnumerator DropAfter(float Seconds) 
	{
		yield return new WaitForSeconds(Seconds);
		myRigidbody.useGravity = true;
        myRigidbody.isKinematic = false;
		Destroy (bridge, 1);
	}
}