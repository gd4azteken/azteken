﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
/// <summary>
/// Represents what happends when collecting a callender piece
/// @author Jan Julius
/// </summary>
public class CallenderPiece : MonoBehaviour {

    /// <summary>
    /// Player component of the player 
    /// </summary>
    Player player;

    /// <summary>
    /// id of the callender piece counting these goes like a callender too
    /// </summary>
    [Header("Insert callenerID here (id = month start at 0)")]
    public int id;

    private float rotationSpeed = 10;

    void Start()
    {
        GetComponent<BoxCollider>().isTrigger = true;
        GetComponentInChildren<SpriteRenderer>().sprite = Resources.Load<Sprite>(Settings.SpritePath + id);
    }

    void Update()
    {

        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + Time.deltaTime * rotationSpeed, transform.eulerAngles.z);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            //sets the player variable to the component Player on the player when the player hits the trigger on this game object
            player = other.GetComponent<Player>();

            //runs a method in the player component allowing the player to collect the callender piece.
            player.CollectCallender(id, Settings.MonthNames[id]);

            //destroys the gameobject
            Destroy(this.gameObject);
        }
    }
}
