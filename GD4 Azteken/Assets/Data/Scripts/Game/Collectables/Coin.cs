﻿    using UnityEngine;
using System.Collections;

/// <summary>
/// represents the coin collectable
/// @ author Jan Julius
/// </summary>
public class Coin : MonoBehaviour {

    [Header("How fast the coin (or other object) rotates")]
    public float rotationSpeed = 10;

    void Start()
    {
        transform.position = new Vector3(Settings.levelMiddleBlocksLocation, transform.position.y, transform.position.z);
    }

    void Update()
    {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + Time.deltaTime*rotationSpeed, transform.eulerAngles.z);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            var p = other.GetComponent<Player>();
            int c = p.GetCurrency();
            p.SetCurrency(c += 1);
            Destroy(this.gameObject);
        }
    }
}
