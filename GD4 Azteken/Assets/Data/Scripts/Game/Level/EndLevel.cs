﻿using UnityEngine;
using System.Collections;

/// <summary>
/// ends the level
/// @author Jan Julius
/// </summary>
public class EndLevel : MonoBehaviour {

    [Header("Level that you would like to load as the player makes contact with this object")]
    public int level;

    public MainMenu mainmenu;

    void Start()
    {
        mainmenu = GameObject.Find("GUI").GetComponent<MainMenu>();
        if (gameObject.GetComponent<Collider>() == null)
        {
            BoxCollider objcoll = gameObject.AddComponent<BoxCollider>();
            objcoll.isTrigger = true;

        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            //do something here if you want
            PlayerPrefs.SetInt("Level" + level+1 + "Unlocked", 1);
            GameManager.SavePlayerData(other.gameObject.GetComponent<Player>());
            mainmenu.loadlevel(level);
        }
    }
}
