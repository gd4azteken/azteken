﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Assets.Data.Scripts.Game.Level
{
    /// <summary>
    /// represents the teleporter component allowing the player to go from one place to another WIP
    /// @author Jan Julius
    /// </summary>
    [RequireComponent(typeof(BoxCollider))]
    class Teleporter : MonoBehaviour
    {
        private BoxCollider colliderEntry, colliderExit;

        public Vector3 TeleportEntry, TeleportExit;

        private bool canTeleport = true;

        [Header("Time between activation of returning value doesnt do anything if crossteleport is off")]
        public float coolDownTime;

        [Header("1 way teleport or 2 way teleport")]
        public bool allowCrossTeleport = false;

        public void Start()
        {
            colliderEntry = GetComponent<BoxCollider>();
            colliderEntry.isTrigger = true;
            transform.position = new Vector3(Settings.levelMiddleBlocksLocation, transform.position.y,
                transform.position.z);

            if (allowCrossTeleport)
            {
                colliderExit = gameObject.AddComponent<BoxCollider>();
                colliderExit.center = new Vector3(TeleportExit.x - transform.position.x,
                    TeleportExit.y - transform.position.y, TeleportExit.z - transform.position.z);
                colliderExit.isTrigger = true;
            }

        }

        public void OnDrawGizmos()
        {
            Gizmos.DrawIcon(TeleportEntry, 1.ToString(), true);
            Gizmos.DrawIcon(TeleportExit, 2.ToString(), true);
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.name == "Player")
            {
                GameObject player = other.gameObject;
                if (!allowCrossTeleport)
                {
                    Teleport(TeleportExit, player);
                }
                else if (allowCrossTeleport)
                {
                    float a = Vector3.Distance(other.transform.position, TeleportExit);
                    float b = Vector3.Distance(other.transform.position, TeleportEntry);

                    if (a < b)
                    {
                        Teleport(TeleportEntry, player);
                    }
                    else if (b < a)
                    {
                        Teleport(TeleportExit, player);
                    }
                }
            }
        }

        /// <summary>
        /// Teleports the player to the location
        /// </summary>
        /// <param name="loc">vector3 location</param>
        /// <param name="player">player gameobject</param>
        public void Teleport(Vector3 loc, GameObject player)
        {
            if (canTeleport)
            {
                player.transform.position = loc;

                canTeleport = false;

                StartCoroutine("Cooldown");
            }
        }

        IEnumerator Cooldown()
        {
            yield return new WaitForSeconds(coolDownTime);
            canTeleport = true;
        }
    }
}
