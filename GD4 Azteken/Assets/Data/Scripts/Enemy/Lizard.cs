﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;
using Assets.Data.Scripts.Enemy;
/// <summary>
/// Represents the lizard enemy inherits from the Enemy class
/// @author Jan Julius
/// </summary>
[RequireComponent(typeof(Rigidbody))]


public class Lizard : Enemy
{
    /// <summary>
    /// 3d model of the lizard stored in children objects
    /// </summary>
    public GameObject ObjectModel;

    /// <summary>
    /// renderer of the objectModels renderer
    /// </summary>
    private Renderer _renderer;

    /// <summary>
    /// animator of the objectmodels animator component
    /// </summary>
    private Animator _anim;

    /// <summary>
    /// boolean to determine if the lizard is dead
    /// </summary>
    private bool dead;

    private bool canDamage = true;

    /// <summary>
    /// timer for destorying the lizard after killing it.
    /// </summary>
    private float timer = 0;

    private bool Jumpable = true;

    public void Start()
    {
        if (Jumpable)
        {
            if (GetComponent<Jumpable>() == null)
            {
                gameObject.AddComponent<Jumpable>();
            }
        }
        _anim = ObjectModel.GetComponent<Animator>();
        _renderer = GetComponent<Renderer>();
    }

    public void FixedUpdate()
    {
        Move(MoveLeft);
        var rayDirection = getDirectionRay(!MoveLeft);
        RaycastHit hit;
        float rayLength = 0.5f;
        if (Physics.Raycast(transform.position, rayDirection, out hit, rayLength))
        {
            if (!hit.collider.isTrigger)
            {
                if (hit.collider.name == "Player")
                {
                    MoveLeft = !MoveLeft;
                    SetRotation(MoveLeft);
                }
                else
                {
                    MoveLeft = !MoveLeft;
                    SetRotation(MoveLeft);
                }
            }
        }
        if (_anim.GetCurrentAnimatorStateInfo(0).IsName("Death"))
        {

            dead = true;
        }
        if (dead)
        {
            timer += Time.deltaTime;
            if (timer > 0.5f)
            {
                transform.position += Vector3.down * 1f * Time.deltaTime;
                if (timer > 2f)
                {
                    Destroy(this.gameObject);
                }
            }
        }
    }
   // public void OnTriggerEnter(Collider other)
   // {
   //     if (other.name == "Player")
   //     {
   //         Player player = other.gameObject.GetComponent<Player>();
   //         RaycastHit hit;
   //         Vector3 rayDirection = transform.TransformDirection(Vector3.up);
   //         float rayLength = 10f;
   //
   //         Throw(Physics.Raycast(
   //                transform.position, rayDirection, out hit, rayLength),
   //                Physics.Raycast(
   //                new Vector3(transform.position.x, transform.position.y,
   //                    transform.position.z - _renderer.bounds.size.z / 1.25f), rayDirection, out hit, rayLength),
   //                Physics.Raycast(
   //                new Vector3(transform.position.x, transform.position.y,
   //                    transform.position.z - _renderer.bounds.size.z / -1.25f), rayDirection, out hit, rayLength),
   //                    player, 5.5f); 
   //     }
   // }

    /// <summary>
    /// sets the rotation depending on the orientation of the Lizard
    /// </summary>
    /// <param name="m"></param>
    public void SetRotation(bool m)
    {
        if (canDamage)
        {
            if (m)
            {
                ObjectModel.transform.eulerAngles = new Vector3(0, 0, 0);
            }
            else
            {
                ObjectModel.transform.eulerAngles = new Vector3(0, 180, 0);
            }
        }
    }

    public void Kill()
    {
        canDamage = false;
        this.gameObject.GetComponent<Rigidbody>().useGravity = false;
        this.gameObject.GetComponent<Collider>().enabled = false;
        base.Speed = 0;
        _anim.SetBool("Dead", true);
    }

    public void setCanDamage(bool candamage)
    {
        canDamage = candamage;
    }

    public bool getCanDamage()
    {
        return canDamage;
    }
}