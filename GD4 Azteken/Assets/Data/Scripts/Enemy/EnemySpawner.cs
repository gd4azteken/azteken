﻿using UnityEngine;
using System.Collections;

/// <summary>
/// represents the enemy spawner
/// @author Jan Julius
/// </summary>
[RequireComponent(typeof(BoxCollider))]
public class EnemySpawner : MonoBehaviour
{
    /// <summary>
    /// gameobject of enemy that is going to be spawned, loaded in from resource folder in runtime
    /// </summary>
    public GameObject Enemy;

    /// <summary>
    /// boxcollider component of enemyspawner used as trigger
    /// </summary>
    private BoxCollider _boxcollider;

    /// <summary>
    /// ID IS DEPENDANT ON SPAWNER TYPE
    /// </summary>
    [Header("0: NORMAL | 1: SHOOTER")]
    public int Id;

    /// <summary>
    /// after the enemy is spawned the enemy gameobject gets stored here.
    /// </summary>
    private GameObject _myEnemy;

    /// <summary>
    /// point the enemy gameobject spaws at
    /// </summary>
    public Vector3 SpawnPoint;
    [Header("If ticked, spawns enemy in the middle of the object")] public bool SpawnInMiddle = false;

    /// <summary>
    /// boolean to determine wether spawning is on or off
    /// </summary>
    private bool spawning = false;

    /// <summary>
    /// spawns objects from the start is set to true
    /// </summary>
    [Header("Always spawns, does not require player to enter collider")]
    public bool SpawnFromStart = false;

    /// <summary>
    /// if the id is set to 1 the bullet will only shoot to the left
    /// </summary>
    [Header("For bullet enemy, only shoot to left ignore player pos")]
    public bool OnlyShootLeft = false;

    /// <summary>
    /// if id is set to 1 the bullet will only shoot to the right
    /// </summary>
    [Header("For bullet enemy, only shoot to right ignore player pos")]
    public bool OnlyShootRight = false;

    /// <summary>
    /// If this is set to true the player can jump on the spawned enemy (usually true)
    /// </summary>
    [Header("Can the player jump on the targeted enemy?")] 
    public bool CanJumpOnMyEnemy = true;

    public void Start()
    {
        transform.position = new Vector3(Settings.levelMiddleBlocksLocation, transform.position.y, transform.position.z);
        _boxcollider = gameObject.GetComponent<BoxCollider>();
        _boxcollider.isTrigger = true;
        _boxcollider.size = new Vector3(5, 15, 30);
        gameObject.transform.position = new Vector3(0, gameObject.transform.position.y, transform.transform.position.z);

        Enemy = Resources.Load<GameObject>(Settings.EnemyPrefabPath + Id);

        if (SpawnInMiddle)
        {
            SpawnPoint = transform.position;
        }
        if (SpawnFromStart)
        {
            switch (Id)
            {
                case 0:
                    SpawnEnemy(Id, SpawnPoint);
                    break;
                case 1:
                    if (!spawning)
                    {
                        StartCoroutine("ContinousSpawning");
                        spawning = true;
                    }
                    break;
            }

        }
    }

    public void OnDrawGizmos()
    {
        //draws spawn point icon
        Gizmos.DrawIcon(SpawnPoint, 0.ToString(), true);

    }

    public void OnTriggerEnter(Collider other)
    {   
        if (other.name == "Player")
        {
            switch (Id)
            {
                case 0:
                    SpawnEnemy(Id, SpawnPoint);
                    break;
                case 1:
                    if (!spawning)
                    {
                        StartCoroutine("ContinousSpawning");
                        spawning = true;
                    }
                    break;
            }
        }
    }

    IEnumerator ContinousSpawning()
    {
        SpawnEnemy(Id, transform.position);
        yield return new WaitForSeconds(2);
        StartCoroutine("ContinousSpawning");    
    }

    /// <summary>
    /// spawns the enemy
    /// </summary>
    /// <param name="enemyId">id of the enemy declared i nthe inspector of this script</param>
    /// <param name="spawnPoint">vector3 of where to spawn the enemy</param>
    protected void SpawnEnemy(int enemyId, Vector3 spawnPoint)
    {
        switch (enemyId) 
        { 
            case 0:
                if (_myEnemy == null)
                {
                    _myEnemy = (GameObject) Instantiate(Enemy, spawnPoint, Quaternion.identity);
                }
                if (_myEnemy.activeSelf)
                {
                    return;
                }
                else
                {
                    _myEnemy.GetComponent<Lizard>().Jumpable = CanJumpOnMyEnemy;
                    _myEnemy.SetActive(true);
                    _myEnemy.transform.position = spawnPoint;
                }
        break;
            case 1:
        if (_myEnemy == null)
        {
            _myEnemy = (GameObject)Instantiate(Enemy, spawnPoint, Quaternion.identity);
            //can i jump on the enemy?
            _myEnemy.GetComponent<BulletEnemy>().Jumpable = CanJumpOnMyEnemy;
            if (OnlyShootLeft)
            {
                _myEnemy.GetComponent<BulletEnemy>().OnlyMoveLeft = OnlyShootLeft;
            }
            else if (OnlyShootRight)
            {
                _myEnemy.GetComponent<BulletEnemy>().OnlyMoveRight = OnlyShootRight;
            }

        }
        break;
        }
    }


}
