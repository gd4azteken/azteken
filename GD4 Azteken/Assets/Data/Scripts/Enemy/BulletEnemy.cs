﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;
using Assets.Data.Scripts.Enemy;

/// <summary>
/// represents the bullet enemy inherited from the enemy class
/// @author Jan Julius
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class BulletEnemy : Enemy
{

    private GameObject _player;

    private Rigidbody _rigidbody;
    private Renderer _renderer;
    private BoxCollider _boxCollider;

    public GameObject model;

    [HideInInspector]
    public bool OnlyMoveLeft = false;
    [HideInInspector]
    public bool OnlyMoveRight = false;

    [Header("Killable by jumping on it?")]
    public bool Jumpable = true;

    public void Start()
    {
        if (Jumpable)
        {
            if (GetComponent<Jumpable>() == null)
            {
                gameObject.AddComponent<Jumpable>();
            }
        }
        StartCoroutine("FallAfter");
        _rigidbody = GetComponent<Rigidbody>();
        _renderer = GetComponent<Renderer>();
        _boxCollider = GetComponent<BoxCollider>();
        _rigidbody.useGravity = false;
        _player = GameObject.Find("Player");
        if (OnlyMoveLeft)
        {
            MoveLeft = true;
        }
        else if (OnlyMoveRight)
        {
            MoveLeft = false;
        } 
        else if (_player.transform.position.z >= transform.position.z)
        {
            MoveLeft = true;
        }
        else
        {
            MoveLeft = false;
        }
        if (MoveLeft)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        Destroy(gameObject, 3);
    }

    IEnumerator FallAfter()
    {
        yield return new WaitForSeconds(1.4f);
        base.Speed = base.Speed/2;
        BoxCollider collisionCollider = gameObject.AddComponent<BoxCollider>();
        Physics.IgnoreCollision(collisionCollider, _player.GetComponent<BoxCollider>());
        _rigidbody.useGravity = true;
        _boxCollider.isTrigger = false;
        yield return new WaitForSeconds(0.6f);
        base.Speed = base.Speed/4;
        _boxCollider.enabled = false;
        collisionCollider.enabled = false;
    }


}
