﻿using UnityEngine;

namespace Assets.Data.Scripts.Enemy
{
    /// <summary>
    /// Wether you can jump on the enemy or not
    /// @author Jan Julius
    /// </summary>
    class Jumpable : MonoBehaviour
    {

        private Renderer _renderer;

        private Rigidbody _rigidbody;

        private BulletEnemy bulletenemy;
        private Lizard lizardenemy;

        private float bX = 1.25f;

        void Start()
        {
            _renderer = GetComponent<Renderer>();
            _rigidbody = GetComponent<Rigidbody>();

            if (GetComponent<BulletEnemy>() != null)
            {
                bulletenemy = GetComponent<BulletEnemy>();
            }
            if (GetComponent<Lizard>() != null)
            {
                lizardenemy = GetComponent<Lizard>();
            }
        }

        public void OnTriggerEnter(Collider other)
        {
            if (other.name == "Player")
            {
                Player player = other.gameObject.GetComponent<Player>();
                RaycastHit hit;
                Vector3 rayDirection = transform.TransformDirection(Vector3.up);
                float rayLength = 10f;
                if (bulletenemy != null)
                {
                    bX = 0f;
                }
                if (lizardenemy != null)
                {
                    bX = 1.25f;
                    if (lizardenemy.getCanDamage())
                    {
                        Throw(Physics.Raycast(
                            transform.position, rayDirection, out hit, rayLength),
                            Physics.Raycast(
                                new Vector3(transform.position.x, transform.position.y,
                                    transform.position.z - _renderer.bounds.size.z/bX), rayDirection, out hit, rayLength),
                            Physics.Raycast(
                                new Vector3(transform.position.x, transform.position.y,
                                    transform.position.z - -_renderer.bounds.size.z/-bX), rayDirection, out hit,
                                rayLength),
                            player, 5.5f);
                    }
                }
            }
        }

        public void OnDrawGizmos()
        {
            Gizmos.DrawRay(new Vector3(transform.position.x, transform.position.y,
                           transform.position.z), Vector3.up);
            Gizmos.DrawRay(new Vector3(transform.position.x, transform.position.y, transform.position.z - _renderer.bounds.size.z / bX), Vector3.up);
            Gizmos.DrawRay(
                new Vector3(transform.position.x, transform.position.y, transform.position.z - -_renderer.bounds.size.z / -bX),
                Vector3.up);

        }

        /// <summary>
        /// throws a method to see if the player hit the enemy on the top and take damage from any other direction
        /// </summary>
        /// <param name="a">left top side</param>
        /// <param name="b">middle top side</param>
        /// <param name="c">right top side</param>
        /// <param name="p">player component</param>
        /// <param name="f">amount of force (when hit)</param>
        public void Throw(bool a, bool b, bool c, Player p, float f)
        {
            print("Throw results" + a + b + c);
            if (a || b || c && lizardenemy != null)
            {
                p.ApplyForce(new Vector3(0, f, 0), 15, ForceMode.Impulse);
            }
            if (a)
            {
                _rigidbody.useGravity = true;
                KillSomething();
                return;
            }
            else if (b)
            {
                _rigidbody.useGravity = true;
                KillSomething();
                return;
            }
            else if (c)
            {
                _rigidbody.useGravity = true;
                KillSomething();
                return;
            }
            else
            {
                HurtPlayer(p);
            }
        }

        private void KillSomething()
        {
            if (lizardenemy != null)
            {
                lizardenemy.Kill();
            }  
        }

        public void HurtPlayer(Player p)
        {
            if (lizardenemy != null)
            {
                if (p.transform.position.z >= transform.position.z)
                {
                    p.TakeDamage(1, 1, 9.2f);
                }
                else
                {
                    p.TakeDamage(1, -1, 9.2f);
                }
                lizardenemy.MoveLeft = !lizardenemy.MoveLeft;
                lizardenemy.SetRotation(lizardenemy.MoveLeft);
            }
            if (bulletenemy != null)
            {
                p.TakeDamage(1, 0, 0);
                Destroy(gameObject);
            }
        }
        
    }
}
