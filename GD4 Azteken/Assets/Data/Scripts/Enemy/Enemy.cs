﻿using UnityEngine;
using System.Collections;

/// <summary>
/// represents enemy
/// @author Jan Julius
/// </summary>
[RequireComponent(typeof (BoxCollider))]
public class Enemy : MonoBehaviour
{
    public float Speed;

    public bool MoveLeft;

    public bool Jumpable;

    private Player player;

    public void Update()
    {
        Move(MoveLeft);
    }

    /// <summary>
    /// returns the direction the ray is going 
    /// </summary>
    /// <param name="moveLeft">boolean what way the player moves</param>
    /// <returns>returns vector3 of the direction ray</returns>
    public Vector3 getDirectionRay(bool moveLeft)
    {
        if (moveLeft)
        {
            return Vector3.back;
        }
        else
        {
            return Vector3.forward;
        }
    }

    /// <summary>
    /// Enemy moving using transform.translate
    /// </summary>
    /// <param name="moveLeft">move direction by boolean</param>
    public void Move(bool moveLeft)
    {
        if (moveLeft)
        {
            transform.Translate(new Vector3(0, 0, 1)*Speed);
        }
        else
        {
            transform.Translate(new Vector3(0, 0, -1)*Speed);
        }
    }

    /// <summary>
    /// returns boolean value for move direction
    /// </summary>
    /// <returns>false or true depending on the MoveLeft boolean</returns>
    public bool moveDirection()
    {
        if (MoveLeft)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
