﻿using System;
using UnityEngine;
using System.Collections;
using System.Reflection;
using UnityEngine.EventSystems;

#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.
#pragma warning disable 0414 // private field assigned but not used.
#pragma warning disable 0108 // private field assigned but not used.

/*
 * @author Jan Julius
 * @author Thimo
 */
/// <summary>
/// main player component handles the following:
/// animation, jumping, moving, turning the player
/// also has: locking the player, health, max health and currency
/// make sure to place the player model (preferably under the player which contains the rigidbody and boxcollider)
/// also loadable from inspetor but instantiating is needed then.
/// </summary>
public class Player : MonoBehaviour
{
    //private variables
    private int _health = 3, 
                _maxHealth = 3, 
                _currency = 0;

    [Header("IMPORTANT: INSERT MODEL OF PLAYER HERE")]
    public GameObject Playermodel;

    private float _moveSpeed = 10,
                  _jumpSpeed = 6;

    private float _timeToLock, 
                  _lockTime;

    private bool _hittingGround, 
                 _locking,
                 _jumping;


    private float jumpTimer = 0;

    private Rigidbody _rigidbody;
    private Collider _collider;
    private Animator _anim;

    public GameInterface Gameinterface;

    //static variables
    public static bool Moveleft,
                       Moveright;

    public void Start()
    {
        if (Playermodel == null)
        {
            Playermodel = GameObject.FindGameObjectWithTag("PlayerModel");
        }
        if (Gameinterface == null)
        {
            GameObject.FindGameObjectWithTag("GameInterface");
        }
        _anim = Playermodel.GetComponent<Animator>();
        _collider = GetComponent<Collider>();
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.velocity = new Vector3(0, 0, 0);
        SetHealth(3);
        ButtonMoveLeft(false); ButtonMoveRight(false);
    }

    public void Awake()
    {
        Gameinterface = GameObject.Find("UI").GetComponentInChildren<GameInterface>();
    }

    public void Update()
    {
        if (GetJump())
        {
            Jump();
        }
        if (_locking)
        {
            _lockTime += Time.deltaTime;
            if (_lockTime >= _timeToLock)
            {
                _locking = false;
            }
            return;
        }
        if (!_locking)
        {
            //move information left (by button feedback) see function buttonMoveLeft
            switch (Moveleft)
            {
                case true:
                    MoveLeft();
                    break;
                case false:
                    break;
            }

            //move information right (by button feedback) see function buttonMoveRight
            switch (Moveright)
            {
                case true:
                    MoveRight();
                    break;
                case false:
                    break;
            }

            //raycast for jumping


            //only for dev works the same as buttons
            if (Input.GetKeyDown(KeyCode.A))
            {
                ButtonMoveLeft(true);
            }
            if (Input.GetKeyUp(KeyCode.A))
            {
                ButtonMoveLeft(false);
                
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                ButtonMoveRight(true);
            }
            if (Input.GetKeyUp(KeyCode.D))
            {
                ButtonMoveRight(false);
                
            }

           if (Input.GetKey(KeyCode.W))
           {
               Jump();  
           }
            if (Input.GetKeyUp(KeyCode.W))
            {
                ResetJumpTimer(0);
            }
        }
        if (_anim.GetCurrentAnimatorStateInfo(0).IsName("Player_Jumping"))
        {
            SetAnimation(2, false, false);
        }   
    }


    /// <summary>
    /// register collecing a calender piece
    /// </summary>
    /// <param name="id">piece id by month 0 = january etc</param>
    public void CollectCallender(int id, string name)
    {
        PlayerPrefs.SetInt("CallenderPiece" + id, 1);
        int collected = 0;
        for (int i = 0; i < 12; i++)
        {
            if (PlayerPrefs.GetInt("CallenderPiece" + i) == 1)
            {
                collected++;
            }
        }
        print("Collected: " + collected);
        Gameinterface.UpdateInterface(2, collected);
    }

    /// <summary>
    /// resets movespeed on collision entereing
    /// </summary>
    void OnCollisionEnter()
    {
        SetMovespeed(5);
    }

    /// <summary>
    /// sets lower movespeed when airborne
    /// </summary>
    void OnCollisionExit()
    {
        SetMovespeed(5);
    }
    /// <summary>
    /// triggered by button feedback for moving left
    /// </summary>
    /// <param name="l">add force (true or false boolean)</param>
    public void ButtonMoveLeft(bool l)
    {
        Moveleft = l;
        if (!l)
        {
            MoveButtonReleased();
        }
    }
    
    /// <summary>
    /// triggered by button feedback for moving right
    /// </summary>
    /// <param name="r">add force (true or false boolean)</param>
    public void ButtonMoveRight(bool r)
    {
        Moveright = r;
        if (!r)
        {
            MoveButtonReleased();
        }
    }

    /// <summary>
    /// to ensure idle animation reactivating after the player doesnt move anymore call this method when the pointer exits the movement buttons on the screen
    /// </summary>
    public void MoveButtonReleased()
    {
        SetAnimation(1, false, false);
    }

    /// <summary>
    /// Jumping
    /// </summary>
    private void Jump()
    {
        SetAnimation(2, false, true);
        jumpTimer += Time.deltaTime;
        RaycastHit hit;
        Vector3 rayDirection = transform.TransformDirection(Vector3.down);
        float rayLength = 1f;
        if (Physics.Raycast(transform.position, rayDirection, out hit, rayLength))
        {
            print("coll" + hit.collider.name);
            if (hit.collider.tag == "Ground")
            {
                print("hitgorund"+ hit.collider.name);
                jumpTimer = 0;
                if (jumpTimer <= 59)
                {
                    jumpTimer = 59;
                    _rigidbody.velocity = new Vector3(0, GetJumpSpeed(), 0);
                }
            }
        }
        else
        {

            if (jumpTimer >= 59.1 && jumpTimer <= 59.2)
            {
                _rigidbody.velocity = new Vector3(0, GetJumpSpeed() , 0);
            }
            else
            {
                return;
            }
        }
    }
    /// <summary>
    /// character moving left these are not public due to GUI seeing these and mr THIMO missusing my methods =.=
    /// </summary>
    void MoveLeft()
    {
        if (Math.Abs(_rigidbody.velocity.y) < 1)
        {
            SetAnimation(2, false, false);
            SetAnimation(1, false, true);
        }
        Playermodel.transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z);
        transform.position += Vector3.forward * _moveSpeed * Time.deltaTime;
    }
    
    /// <summary>
    /// character moving right these are not public due to GUI seeing these and mr THIMO missusing my methods =.=
    /// </summary>
    void MoveRight()
    {
        if (Math.Abs(_rigidbody.velocity.y) < 1)
        {
            SetAnimation(2, false, false);
            SetAnimation(1, false, true);
        }
        Playermodel.transform.eulerAngles = new Vector3(transform.eulerAngles.x, 180, transform.eulerAngles.z);
        transform.position += Vector3.back * _moveSpeed * Time.deltaTime;
    }
    /// <summary>
    /// Healing the player
    /// </summary>
    /// <param name="heal">Heal for what amount</param>
    public void Heal(int heal)
    {
        if (_health <= _maxHealth)
        {
            _health += heal;
        }
    }

    /// <summary>
    /// How much health the player loses
    /// </summary>
    /// <param name="damage">how much damage is dealt</param>
    /// <param name="direction">what direction the player goes when damage gets taken calculate this from the playerposx and enemypos</param>
    /// <param name="force">how far the player gets hit away</param>
    public void TakeDamage(int damage, float direction, float force)
    {
        Lock(0.5f);
        if (direction <= 0)
        {
            ApplyForce(new Vector3(0, 0.3f, -0.5f), force, ForceMode.Impulse);
        }
        else
        {
            ApplyForce(new Vector3(0, 0.3f, 0.5f), force, ForceMode.Impulse);
        }
        
        SetHealth(GetHealth() - damage);
    }

    /// <summary>
    /// adds fors to the character
    /// </summary>
    /// <param name="dir">vecto3 of fiorce direction</param>
    /// <param name="force">how much force gets applied</param>
    /// <param name="mode">force mode</param>
    public void ApplyForce(Vector3 dir, float force, ForceMode mode)
    {
        _rigidbody.AddForce(new Vector3(dir.x, dir.y, dir.z)* force, mode);
    }
    /// <summary>
    /// returns the health value
    /// </summary>
    /// <returns>health</returns>
    public int GetHealth()
    {
        return _health;
    }
    /// <summary>
    /// set the player health
    /// </summary>
    /// <param name="hp">how much health should the players health be</param>
    public void SetHealth(int hp)
    {
        _health = hp;
        Gameinterface.UpdateInterface(0, hp);
        if (_health <= 0)
        {
            Die();
        }
    }
    /// <summary>
    /// returns maximum health
    /// </summary>
    /// <returns>maxHealth</returns>
    public int GetMaxHealth()
    {
        return _maxHealth;
    }
    /// <summary>
    /// sets the players maximum health value
    /// </summary>
    /// <param name="mhp">how much should the max health value be</param>
    public void SetMaxHealth(int mhp)
    {
        _maxHealth = mhp;
    }
    /// <summary>
    /// returns currecny int
    /// </summary>
    /// <returns>currency</returns>
    public int GetCurrency()
    {
        return _currency;
    }

    /// <summary>
    /// sets currency
    /// </summary>
    /// <param name="c">how much currency</param>
    public void SetCurrency(int c)
    {
        _currency = c;
        Gameinterface.UpdateInterface(1, c);
    }
    /// <summary>
    /// returns movespeed
    /// </summary>
    /// <returns>_moveSpeed</returns>
    public float GetMovespeed()
    {
        return _moveSpeed;
    }
    /// <summary>
    /// sets movespeed
    /// </summary>
    /// <param name="movespeed">movespeed</param>
    public void SetMovespeed(float movespeed)
    {
        _moveSpeed = movespeed;
    }

    /// <summary>
    /// returns the jump speed
    /// </summary>
    /// <returns>JumpSpeed</returns>
    public float GetJumpSpeed()
    {
        return _jumpSpeed;
    }
    /// <summary>
    /// sets the jumpspeed
    /// </summary>
    /// <param name="jumpspeed">jumpspeed to set to</param>
    public void SetJumpSpeed(float jumpspeed)
    {
        _jumpSpeed = jumpspeed;
    }

    public bool GetJump()
    {
        return _jumping;
    }

    public void SetJump(bool jump)
    {
        _jumping = jump;
    }

    public void ResetJumpTimer(float t)
    {
        jumpTimer = t;
    }
    /// <summary>
    /// locks the player (dissalows any movement)
    /// </summary>
    /// <param name="time">how long the player should be locked for</param>
    public void Lock(float time)
    {
        _locking = true;
        _lockTime = 0;
        _timeToLock = time;
    }

    /// <summary>
    /// sets animation of the player
    /// </summary>
    /// <param name="id">refers to the id of the animation, done on sequence of the animator on the player model see animator</param>
    /// <param name="r">resest other animations (usually you want this)</param>
    public void SetAnimation(int id, bool r, bool set)
    {
        /*ANIMATION IS DONE BY NUMBER
        *
         * 0: IDLE
         * 1: WALKING
         * 2: JUMPING
         */
        if (r)
        {
            for (int i = 0; i < 2; i++)
            {
                _anim.SetBool(i.ToString(), false);
            }
        }
        _anim.SetBool(id.ToString(), set);
    }
    /// <summary>
    /// when the player dies
    /// </summary>
    public void Die()
    {
        SetHealth(3);
        GameManager.SavePlayerData(GetComponent<Player>());
        GameManager.LoadPlayerData(GetComponent<Player>());
        Application.LoadLevel(Application.loadedLevel);
    }
}