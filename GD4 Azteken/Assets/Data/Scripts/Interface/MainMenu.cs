﻿using UnityEngine;
using System.Collections;

/// <summary>
/// represents the MainMenu methods
/// @author Jan Julius
/// @author Thimo
/// </summary>
public class MainMenu : MonoBehaviour 
{
    /// <summary>
    /// starts the game by loading Main0
    /// </summary>
	public void StartGame()
	{
		Application.LoadLevel ("Main0");
	}

    /// <summary>
    /// loads level depending on int
    /// </summary>
    /// <param name="level">level to load int</param>
    public void loadlevel(int level)
    {
        print("Attempting to load level: " + level);
        if (level == 6)
        {
            int levelCounter = 0;
            for (int i = 0; i < 12; i++)
            {
                print("1attempting...");
                if (PlayerPrefs.GetInt("CallenderPiece" + i) == 1)
                {
                    print("2attempting...");
                    levelCounter += 1;
                }
            }
            if (levelCounter == 12)
                {
                    Application.LoadLevel("Main" + level);
                }
                else
                {
                    //TODO: do something special here make scene to let the player know that they do not apply for the last level
                    Application.LoadLevel("Start");
                    print("Does not apply for last level, pieces collected: " + levelCounter);
                }
        }
        else
        {
            Application.LoadLevel("Main" + level);
        }
    }

    /// <summary>
    /// loads level select scene
    /// </summary>
	public void LevelSelect()
	{
		Application.LoadLevel("LevelSelection");
	}

    /// <summary>
    /// loads options scene
    /// </summary>
	public void Options()
	{
		Application.LoadLevel ("Options");
	}

    /// <summary>
    /// exits the game
    /// </summary>
	public void ExitGame()
	{
		Application.Quit();
	}

    /// <summary>
    /// returns to start scene
    /// </summary>
	public void Back()
	{
		Application.LoadLevel("Start");
	}

    /// <summary>
    /// resets all playerprefs
    /// </summary>
    public void Reset()
    {
        //TODO: remove this method when teh game is ready for release
        PlayerPrefs.DeleteAll();
    }
}
