﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
#pragma warning disable 0414 // private field assigned but not used.

/// <summary>
/// Handles the buttons in the level select scene, sets the ones active depending on the players progress.
/// @author Jan Julius
/// </summary>
public class LevelSelectHandler : MonoBehaviour 
{

    public Button[] levelButtons;

    private int ONE = 0, TWO = 1, THREE = 2, FOUR = 3, FIVE = 4;

    public void Awake()
    {
        //runs a loop checking the player prefs for unlock values of the levels.
        for (int levelid = ONE; levelid < FIVE; levelid++)
        {
            if (PlayerPrefs.GetInt("Level" + levelid + "Unlocked") == 1)
            {
                levelButtons[levelid].interactable = false;
            }
            else
            {
                levelButtons[levelid].interactable = true;
            }
        }
    }

}
