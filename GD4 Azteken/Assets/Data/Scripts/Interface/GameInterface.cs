﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// represents the game itnerface
/// @author Jan Julius
/// </summary>
public class GameInterface : MonoBehaviour
{
    /// <summary>
    /// holds text component from the interface
    /// </summary>
    public Text[] TextInterfaces;

    /// <summary>
    /// holds images containing gamesprites
    /// </summary>
    public Image[] GameSprites;

    private List<Sprite> _loadedSprites = new List<Sprite>();

    public void Start()
    {
        int amount = 15;
        for (int i = 0; i < amount; i++)
        {
            _loadedSprites.Add(Resources.Load<Sprite>(Settings.SpritePath + i));
        }
        GameSprites[3].sprite = _loadedSprites[14];
        int collected = 0;
        for (int i = 0; i < 12; i++)
        {
            if (PlayerPrefs.GetInt("CallenderPiece" + i) == 1)
            {
                collected++;
            }
        }
        UpdateInterface(0, 3);
        UpdateInterface(2, collected);
    }

    /// <summary>
    /// updates the gameinterface
    /// </summary>
    /// <param name="id">id of the interface 0 is default</param>
    /// <param name="value">value of the interface to pass through</param>
    public void UpdateInterface(int id, int value)
    {
        switch (id) 
        { 
            case 0:

                switch (value)
                {
                    case 0:
                        for (int i = 0; i < 2; i++)
                        {
                            GameSprites[i].sprite = _loadedSprites[13];
                        }
                        break;
                    case 1:
                        GameSprites[0].sprite = _loadedSprites[12];
                        for (int i = 1; i < 2; i++)
                        {
                            GameSprites[i].sprite = _loadedSprites[13];
                        }
                        break;
                    case 2:
                        for (int i = 0; i < 1; i++)
                        {
                            GameSprites[i].sprite = _loadedSprites[12];
                        }
                        GameSprites[2].sprite = _loadedSprites[13];
                        break;
                    case 3:
                        for (int i = 0; i < 3; i++)
                        {
                            GameSprites[i].sprite = _loadedSprites[12];
                        }
                        break;
                    default:
                        break;
                }
            break;
       case 1:
            TextInterfaces[0].text = "x"+value.ToString();
        break;
            case 2:
                GameSprites[4].sprite = _loadedSprites[Random.Range(0, 10)];
                TextInterfaces[1].text = value + "/12";
                break;
        }
       


    }
}
