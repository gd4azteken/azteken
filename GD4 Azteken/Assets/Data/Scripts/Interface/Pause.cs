﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

#pragma warning disable 0414
/// <summary>
/// Ability to pause the game
/// @author Jan Julius
/// </summary>
public class Pause : MonoBehaviour 
{
    /// <summary>
    /// Wether the game is paused or not
    /// </summary>
    private bool _paused = false;

    /// <summary>
    /// Toggle to control the pausing of the game
    /// </summary>
    public Toggle PauseToggle;

    void Start()
    {
        PauseToggle = this.gameObject.GetComponent<Toggle>();
        if (Time.timeScale <= 0)
        {
            Time.timeScale = 1;
        }
    }

    /// <summary>
    /// Switches the toggle between pausing and unpausing the game using timescale
    /// </summary>
    public void PauseGame()
    {
        if (PauseToggle.isOn == true)
        {
            Time.timeScale = 0;
        }
        else if (PauseToggle.isOn == false)
        {
            Time.timeScale = 1;
        }
    }
}
