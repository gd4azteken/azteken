﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class SelectScene : Editor 
{
	[MenuItem("Open Scene/Scenes/Levels/Main0")]
	public static void OpenMain()
	{
		OpenScene("Main0");
	}

	[MenuItem("Open Scene/Scenes/Levels/Main1")]
	public static void OpenMain1()
	{
		OpenScene("Main1");
	}

	[MenuItem("Open Scene/Scenes/Levels/Main2")]
	public static void OpenMain2()
	{
		OpenScene("Main2");
	}

	[MenuItem("Open Scene/Scenes/Levels/Main3")]
	public static void OpenMain3()
	{
		OpenScene("Main3");
	}

	[MenuItem("Open Scene/Scenes/Levels/Main4")]
	public static void OpenMain4()
	{
		OpenScene("Main4");
	}

	[MenuItem("Open Scene/Scenes/LevelSelection")]
	public static void OpenLevelSelection()
	{
		OpenScene("LevelSelection");
	}

	[MenuItem("Open Scene/Scenes/Start")]
	public static void OpenStart()
	{
		OpenScene ("Start");
	}

	static void OpenScene(string name)
	{
		if(EditorApplication.SaveCurrentSceneIfUserWantsTo())
		{
			EditorApplication.OpenScene( "Assets/Data/Scenes/" + name + ".unity");
		}
		else
		{
			EditorApplication.OpenScene( "Assets/Data/Scenes/" + name + ".unity");
		}
	}
}